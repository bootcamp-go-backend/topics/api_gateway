package client

import (
	"api_gateway/config"
	"api_gateway/genproto/protos"
	"google.golang.org/grpc"
)

type IServiceManger interface {
	TodoService() todo_service.TodoServiceClient
	UserService() todo_service.UserServiceClient
}

type grpcClients struct {
	todoService todo_service.TodoServiceClient
	userService todo_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManger, error) {
	connTodoService, err := grpc.Dial(
		cfg.TodoGRPCServiceHost+cfg.TodoGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		todoService: todo_service.NewTodoServiceClient(connTodoService),
		userService: todo_service.NewUserServiceClient(connTodoService),
	}, nil
}

func (g *grpcClients) TodoService() todo_service.TodoServiceClient {
	return g.todoService
}

func (g *grpcClients) UserService() todo_service.UserServiceClient {
	return g.userService
}
