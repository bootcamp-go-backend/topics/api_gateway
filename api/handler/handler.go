package handler

import (
	"api_gateway/config"
	"api_gateway/grpc/client"
)

type Handler struct {
	cfg      config.Config
	services client.IServiceManger
}

func New(cfg config.Config, services client.IServiceManger) Handler {
	return Handler{
		cfg:      cfg,
		services: services,
	}
}
