package handler

import (
	"api_gateway/api/models"
	"api_gateway/genproto/protos"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
)

func (h *Handler) CreateTodo(c *gin.Context) {
	request := models.CreateTodoRequest{}

	if err := c.ShouldBind(&request); err != nil {
		panic(err)
	}

	fmt.Println("request 0101", request)

	h.services.TodoService().Create(context.Background(), &todo_service.CreateTodoRequest{
		Name:     request.Name,
		Deadline: request.Deadline,
		Status:   request.Status,
	})

	c.JSON(200, request)
}

func (h *Handler) CreateUser(c *gin.Context) {
	request := models.CreateUserRequest{}

	if err := c.ShouldBind(&request); err != nil {
		panic(err)
	}

	fmt.Println("request 0101", request)

	resp, err := h.services.UserService().Create(context.Background(), &todo_service.CreateUserRequest{
		Name: request.Name,
		Age:  request.Age,
	})
	fmt.Println(resp, err)

	c.JSON(200, request)
}
