package api

import (
	"api_gateway/api/handler"
	"github.com/gin-gonic/gin"
)

func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	r.POST("/todo", h.CreateTodo)

	r.POST("/user", h.CreateUser)

	return r
}
